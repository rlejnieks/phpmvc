<?php

namespace Core;

/**
 * Basic router for your everyday needs ;)
 * 
 */

 class Router
 {
     
    /**
     * Array of routes
     * @var array
     */
    protected $routes = [];

    /**
     * Params from route
     * @var array
     */
    protected $params = [];

    /**
     * Add route to the routing table
     * @param string $route 
     * @param array $params
     * 
     * @return void
     */
    public function add($route, $params = [])
    {
        // Convert the route to regular expression: escape forward slashes
        $route = preg_replace('/\//', '\\/', $route);

        // Convert variables
        $route = preg_replace('/\{([a-z]+)\}/', '(?P<\1>[a-z-]+)', $route);

        // Converting variables with custom regular expressions
        $route = preg_replace('/\{([a-z]+):([^\}]+)\}/', '(?P<\1>\2)', $route);

        // Add start and end delimiters, and case insensitive flag
        $route = '/^' . $route . '$/i';

        $this->routes[$route] = $params;

    }

    /**
     * Get all the nice routes from the table
     * 
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Match routes to the table, add $params prop if found
     * 
     * @param string $url
     *  
     * @return boolean true if match found
     */
    public function match($url)
    {
        // foreach($this->routes as $route => $params){
        //     if($url === $route){
        //         $this->params = $params;
        //         return true;
        //     }
        // }

        // $regex = "/^(?P<controller>[a-z-]+)\/(?P<action>[a-z-]+)$/";
        foreach($this->routes as $route => $params){
            if(preg_match($route, $url, $matches)){
                // Getting those capture group vals
    
                // $params = [];
    
                foreach($matches as $key => $match){
                    if(is_string($key)){
                        $params[$key] = $match;
                    }
                }
    
                $this->params = $params;
                return true;
            }
        }

        return false;
    }

    /**
     * Get current parameters
     * 
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Dispatch the route, creating the controller object and running the
     * action method
     *
     * @param string $url The route URL
     *
     * @return void
     */
    public function dispatch($url)
    {
        $url = $this->removeQueryStringVariables($url);

        if ($this->match($url)) {
            $controller = $this->params['controller'];
            $controller = $this->convertToCaps($controller);
            // $controller = "App\Controllers\\$controller";
            $controller = $this->getNamespace() . $controller;

            if (class_exists($controller)) {
                $controller_object = new $controller($this->params);

                $action = $this->params['action'];
                $action = $this->convertToCamelCase($action);

                if (is_callable([$controller_object, $action])) {
                    $controller_object->$action();

                } else {
                    throw new \Exception("Method $action (in controller $controller) not found");
                }
            } else {
                throw new \Exception("Controller class $controller not found");
            }
        } else {
            throw new \Exception("No route matched", 404);
        }
    }

    /**
     * Convert the string with hyphens
     *
     * @param string $string The string to convert
     *
     * @return string
     */
    protected function convertToCaps($string)
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
    }

    /**
     * Convert the string with hyphens to camelCase
     *
     * @param string $string The string to convert
     *
     * @return string
     */
    protected function convertToCamelCase($string)
    {
        return lcfirst($this->convertToCaps($string));
    }

    /**
     * Remove queryS sring variables
     * 
     * @param string $url Full url
     * 
     * @return string The string without query variables
     */
    public function removeQueryStringVariables($url)
    {
        if($url != ''){
            $parts = explode('&', $url, 2);

            if(strpos($parts[0], '=') === false){
                $url = $parts[0];
            }else{
                $url = '';
            }
        }

        return $url;
    }

    /**
     * Get namespace for controller class. The namespace in route
     * params is added if present
     * 
     * @return string The request URL
     */
    protected function getNamespace()
    {
        $namespace = 'App\Controllers\\';

        if(array_key_exists('namespace', $this->params)){
            $namespace .= $this->params['namespace'] . '\\';
        }
        return $namespace;
    }

 }