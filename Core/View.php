<?php

namespace Core;

/**
 * View
 * 
 */

 Class View
 {
     /**
      * Render view file 
      *
      * @param string $view The view file
      *
      * @return void
      */
      public static function render($view, $args = [])
      {
          extract($args, EXTR_SKIP);
          
          $file = "../App/Views/$view"; // relative to core dir

          if(is_readable($file)){
              require $file;
          }else{
            throw new \Exception("$file not found");
          }
      }

      /**
       * Render template using Twig
       * 
       * @param string template
       * @param array args
       * 
       * @return void
       */
      public static function renderTemplate($template, $args =[])
      {
          static $twig = null;

          if($twig === null){
            $loader = new \Twig_Loader_Filesystem('../App/Views');
            $twig = new \Twig_Environment($loader);
            
            echo $twig->render($template, $args);
          }


      }
 }