<?php
namespace Core;

/**
 * Base session
 * 
 * 
 */

class Session
 {
    /**
     * Starts the session if not started yet.
     *
     */
    public static function init(){
        if (session_status() == PHP_SESSION_NONE) {     // if (session_id() == '')
            session_start();
        }
    }

    /**
     * set session key and value
     *
     * @param $key
     * @param $value
     *
     */
    public static function set($key, $value){
        $_SESSION[$key] = $value;
    }
    /**
     * get session value by $key
     *
     * @param  $key
     * @return mixed
     *
     */
    public static function get($key){
        return array_key_exists($key, $_SESSION)? $_SESSION[$key]: null;
    }
    /**
     * Unset
     * 
     */
    public static function unset($key){

        if(array_key_exists($key, $_SESSION)){
            $value = $_SESSION[$key];
            $_SESSION[$key] = null;
            unset($_SESSION[$key]);
            return $value;
        }
        
        return null;
    }
 }