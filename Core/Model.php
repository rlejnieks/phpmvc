<?php

namespace Core;

use PDO;
use App\Config;

/**
 * Base model
 * 
 * 
 */

 abstract class Model
 {
    /**
     * Get PDO connection
     * 
     * @return mixed
     */
    protected static function getDB()
    {
        // static $db = null;

        // if($db === null){

            // DSN
            $dsn = 'mysql:host=' . Config::DB_HOST . ';dbname=' .  Config::DB_NAME . ';charset=utf8';
            
            // PDO instance 
            $db = new PDO($dsn, Config::DB_USER, Config::DB_PASSWD);

            $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $db;
            
        // }
        
        
    }
 }