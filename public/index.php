<?php
/**
 * Front controller
 * 
 */

  /**
   * Autoloader
   * 
   */

  require_once '../vendor/autoload.php';

  /**
   * Sessions
   * 
   */
  $session = new Core\Session();
  $session->init();

  /**
   * Error handling
   * 
   */

  set_error_handler('Core\Error::errorHandler');
  set_exception_handler('Core\Error::exceptionHandler');

  /**
  * Routing
  *
  */

  $router = new Core\Router();

  //   $router->add('mvc', ['controller' => 'Mvc', 'action' => 'showQuiz']);
  $router->add('', ['controller' => 'Home', 'action' => 'index']);
  $router->add('{controller}/{action}');
  $router->add('{controller}/{id:\d+}/{action}');
  $router->dispatch($_SERVER['QUERY_STRING']);