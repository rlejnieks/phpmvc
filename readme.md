Simple PHP MVC app
====================

## Requirements
* PHP 7.0
* Apache web server
* SQL DB
* Composer

## Installation

```
composer install
```

All app and db configs can be changed in file

```    
App/Config.php
```

For pretty URLs open

``` 
public/.htaccess
```

and change this line to your liking

```
RewriteBase /[new_app_name_here]/public/
```