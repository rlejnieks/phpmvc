<?php

namespace App;

/**
 * App config
 * 
 * 
 */
class Config
{
    /**
     * DB host
     * @var string
     */
    const DB_HOST = '';

    /**
     * DB name
     * @var string
     */
    const DB_NAME = '';

    /**
     * DB user
     * @var string
     */
    const DB_USER = '';

    /**
     * DB passwd
     * @var string
     */
    const DB_PASSWD = '';

    /**
     * Show errors
     * @var boolean
     */
    const SHOW_ERRORS = true;
}