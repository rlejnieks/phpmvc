<?php

namespace App\Controllers;

use \Core\View;
use \Core\Session;
use \App\Models\User;
/**
 * Home controller
 * 
 */

class Home extends \Core\Controller
{
    /**
     * Show index page
    * 
    * @return void
    */
    public function indexAction()
    {
        // $dataFromDb = User::checkIfRegistered();

        // $userName = Session::get('userName');

        // View::renderTemplate('Home/index.html', [
        //     // 'dataFromDb' => $dataFromDb
        //     'name' => 'My name'
        // ]);
        View::render('Home/index.html', [
            'name' => 'World'
        ]);

    }
}