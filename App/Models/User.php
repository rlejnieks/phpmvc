<?php

namespace App\Models;

use PDO;
use Core\Session;

/**
 * User model
 * 
 */
class User extends \Core\Model
{
    /**
     * Register user
     * 
     */
    public static function register($userName){
        try{
            
            $db = static::getDB();

            $sql = '
                INSERT INTO users (name)
                VALUES (:userName)
            ';

            $stmt = $db->prepare($sql);

            $stmt->execute(['userName' => $userName]);
            
            $id = $db->lastInsertId();

            // closing conn
            $db = null;
            $stmt = null;

            return $id;

        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    /**
     * Check if user is registered
     * 
     */
    public static function checkIfRegistered($userName){
        try{
            
            $db = static::getDB();

            $sql = '
                SELECT users.id, users.name
                FROM users
                WHERE users.name = (:userName)
            ';

            $stmt = $db->prepare($sql);

            $stmt->execute(['userName' => $userName]);
            
            $results = $stmt->fetchAll();

            // closing conn
            $db = null;
            $stmt = null;

            return $results;

        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}